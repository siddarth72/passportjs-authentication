const mongoose = require('mongoose');

// ? Database Connection
exports.connectDatabase = () => {
	mongoose.connect('mongodb://localhost:27017/passportJs')
		.then(() => {
			console.log('Database connected');
		})
		.catch(err => {
			console.log(err);
		});
};

// ? User Schema
const userSchema = new mongoose.Schema({

	username: {
		type: String,
		required: true,
		unique: true
	},

	password: {
		type: String,
		required: true
	},

	name: {
		type: String,
	}

});

exports.User = mongoose.model('User', userSchema);
