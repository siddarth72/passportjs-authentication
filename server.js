const express = require('express');
const ejs = require('ejs');
const app = express();
const passport = require('passport');
const expressSession = require('express-session');
const {
	connectDatabase,
	User } = require('./database');
const {
	initializingPassport,
	authenticatedUser
} = require('./passportConfig');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(expressSession({
	secret: 'keyboardCat',
	resave: false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.set('view engine', 'ejs');

connectDatabase();
initializingPassport(passport);

app.get("/", (req, res) => {
	res.render("index");
});

app.get("/register", (req, res) => {
	res.render("register");
});

app.get("/login", (req, res) => {
	res.render("login");
});

app.post("/register", async (req, res) => {
	const user = await User.findOne({ username: req.body.username });
	if (user)
		return res.status(400).send("User already exists");
	const newUser = await User.create(req.body);
	res.status(201).send(newUser);
});

app.post("/login", passport.authenticate("local", {
	failureRedirect: "/register",
	successRedirect: "/"
}));

app.get("/profile", authenticatedUser, (req, res) => {
	res.send(req.user);
});

app.get("/logout", (req, res) => {
	req.logout();
	res.send("logged out");
});

app.listen(3000, () => {
	console.log(`Server: http://localhost:3000`);
});
